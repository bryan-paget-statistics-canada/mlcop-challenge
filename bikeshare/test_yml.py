#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Apr  2 19:51:22 2022

@author: alexanderel-hajj
"""


from pipeline import get_score
import pandas as pd
print(pd.__version__)
import os

# Get the current working directory
cwd = os.getcwd()

# Print the current working directory
print("Current working directory: {0}".format(cwd))

master_pd = pd.read_csv("leaderboard.csv")
master_dict = master_pd.to_dict('records')


# Get score and unique name
score, unique_name = get_score(pred=None, test_data=None, unique_name="D")

# Create new entry to master leaderboard
new_entry = {unique_name: score}


def check_username(master, new_entry):
    print(f"New Entry: {new_entry}")
    username = [x for x,y in new_entry.items()][0]
    print(username)
    new_score = [y for x,y in new_entry.items()][0]
    print(new_score)
    # if username in [val for sublist in master for val in sublist]:
    if username in [list(y.items())[0][1] for x,y in enumerate(master)]:
        print('name found')
        print('check if score is better')
        print(f'new score: {new_score}')
        # print(f'old score: {score}')
        # if new_score > [val for k,sublist in enumerate(master) for key,val \
        #                 in sublist.items() if key==username][0]:
        if new_score > [list(y.items())[1] for x,y in enumerate(master_dict) \
                        if username in list(y.items())[0]][0][1]:
            print('yes')
            print(new_score)
            index = [x for x,y in enumerate(master_dict) if username in \
                     list(y.items())[0]][0]
            master[index] = {'name' : username, 'score' : new_score}
        else:
            print('waaaa')
        #     print('yes')
        #     print(new_score)
        #     index = [k for k,sublist in enumerate(master) for key,val in sublist.items() if key==username][0]
        #     master[index] = {username : new_score}
    else:
        print('username not in list')
        print(f"New Entry: {new_entry}")
        # tmp = {username: new_score}
        master.append({'name': username, 'score': new_score})
    return master

bshh = check_username(master_dict, new_entry)


bshh_pd = pd.DataFrame(bshh)
bshh_pd = bshh_pd.sort_values(by='score', axis=0, ascending=False)
print("Exporting leaderboard.csv")
bshh_pd.to_csv("leaderboard.csv", index=False)

# bshh_pd.to_csv("/builds/alexelhajj/mlcop-challenge/bikeshare/leaderboard.csv", 
#                index=False)



"""Append to README"""
# def pandas_df_to_markdown_table(df):
#     from IPython.display import Markdown, display
#     fmt = ['---' for i in range(len(df.columns))]
#     df_fmt = pd.DataFrame([fmt], columns=df.columns)
#     df_formatted = pd.concat([df_fmt, df])
#     display(Markdown(df_formatted.to_csv(sep="|", index=False)))

# pandas_df_to_markdown_table(bshh_pd)



